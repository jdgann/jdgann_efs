@extends('app')
@section('content')
    <h1>Customer </h1>

    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <tbody>
            <tr class="bg-info">
            <tr>
                <td>Name</td>
                <td><?php echo ($customer['name']); ?></td>
            </tr>
            <tr>
                <td>Customer ID</td>
                <td><?php echo ($customer['cust_number']); ?></td>
            </tr>
            <tr>
                <td>Address</td>
                <td><?php echo ($customer['address']); ?></td>
            </tr>
            <tr>
                <td>City </td>
                <td><?php echo ($customer['city']); ?></td>
            </tr>
            <tr>
                <td>State</td>
                <td><?php echo ($customer['state']); ?></td>
            </tr>
            <tr>
                <td>Zip </td>
                <td><?php echo ($customer['zip']); ?></td>
            </tr>
            <tr>
                <td>Home Phone</td>
                <td><?php echo ($customer['home_phone']); ?></td>
            </tr>
            <tr>
                <td>Cell Phone</td>
                <td><?php echo ($customer['cell_phone']); ?></td>
            </tr>


            </tbody>
      </table>
    </div>


            <?php
    $stockprice=null;
    $stotal = 0;
    $svalue=0;
    $itotal = 0;
    $ivalue=0;
    $iportfolio = 0;
    $cportfolio = 0;

    $startTotal = 0;
    $currentTotal = 0;
    $performanceTotal = 0;

    $startTotal2 = 0;
    $currentTotal2 = 0;
    $performanceTotal2 = 0;
    ?>
    <br>
    <h2>Stocks </h2>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th> Symbol </th>
                <th>Stock Name</th>
                <th>No. of Shares</th>
                <th>Purchase Price</th>
                <th>Purchase Date</th>
                <th>Original Value</th>
                <th>Current Price</th>
                <th>Current Value</th>
            </tr>
            </thead>

            <tbody>




        @foreach($customer->stocks as $stock)

        <?php
       $ssymbol = $stock->symbol;
        $URL = "http://www.google.com/finance/info?q=NSE:" . $ssymbol;
        $file = fopen("$URL", "r");
        $r = "";
        do {
            $data = fread($file, 500);
            $r .= $data;
        } while (strlen($data) != 0);
        //Remove CR's from ouput - make it one line
        $json = str_replace("\n", "", $r);

        //Remove //, [ and ] to build qualified string
        $data = substr($json, 4, strlen($json) - 5);

        //decode JSON data
        $json_output = json_decode($data, true);
        //echo $sstring, "<br>   ";
        $price = "\n" . $json_output['l'];
        //echo "<br><br>";
       //echo $price;
       //echo ($stock['shares']);

       $start_value = ($stock['purchase_price']) * ($stock['shares']);
       $startTotal += $start_value;
       $current_value = $price * ($stock['shares']);
       $currentTotal += $current_value;
       $performanceTotal = floor(100 * ($currentTotal / $startTotal));
    

        ?>


                <tr>
                <td><?php echo ($stock['symbol']); ?></td>
                <td><?php echo ($stock['name']); ?></td>
                <td><?php echo ($stock['shares']); ?></td>
                <td>$<?php echo ($stock['purchase_price']); ?></td>
                <td><?php echo ($stock['purchased']); ?></td>
                <td>$<?php echo $start_value ?></td>
                <td>$<?php echo $price ?></td>
                <td>$<?php echo $current_value ?></td>




        @endforeach

          </tbody>
        </table>

    <h2>Total Initial Value of Stocks: $<?php echo $startTotal ?></h2>
    <h2>Total Current Value of Stocks: $<?php echo $currentTotal ?></h2>
    <h2>Performance: <?php echo $performanceTotal ?>%</h2>



    </div>

<br>
    <h2>Investments </h2>
    <div class="container">
        <table class="table table-striped table-bordered table-hover">
            <thead>
            <tr class="bg-info">
                <th>Category </th>
                <th>Description</th>
                <th>Acquired Value</th>
                <th>Acquired Date</th>
                <th>Recent Value</th>
                <th>Recent Date</th>
            </tr>
            <tbody>




        @foreach($customer->investments as $investment)

        <?php
       $startTotal2 += ($investment['acquired_value']);
       $currentTotal2 += ($investment['recent_value']);
       if ($startTotal2 > 0) {
         $performanceTotal2 = floor(100 * ($currentTotal2 / $startTotal2));
       } else {
         $performanceTotal2 = 0;
       }
    

        ?>

            <tr>
                <td><?php echo ($investment['category']); ?></td>
                <td><?php echo ($investment['description']); ?></td>
                <td><?php echo ($investment['acquired_value']); ?></td>
                <td><?php echo ($investment['acquired_date']); ?></td>
                <td><?php echo ($investment['recent_value']); ?></td>
                <td><?php echo ($investment['recent_date']); ?></td>
            </tr>

        @endforeach
          </tbody>
        </table>


    <h2>Total Initial Value of Investments: $<?php echo $startTotal2 ?></h2>
    <h2>Total Current Value of Investments: $<?php echo $currentTotal2 ?></h2>
    <h2>Performance: <?php echo $performanceTotal2 ?>%</h2>

    </div>

@stop

